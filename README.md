Dies ist ein Archiv verschiedener interessanter und lehrreicher Praktika, die ich während meines Informatikstudiums an der [Hochschule Darmstadt](https://h-da.de/) absolviert habe.

- [GraphQL vs REST](https://gitlab.com/eggerd_hda/graphql-vs-rest) (6. Semester, Bachelorthesis Appendix)  
  Ein simpler Backend-Server, der CRUD sowohl über eine GraphQL-API als auch über eine RESTful-API bereitstellt. Wurde im Rahmen meiner Bachelorarbeit verwendet, um die Performance und Implementierung von GraphQL- und REST-APIs zu vergleichen

- [Fortgeschrittene Webentwicklung](https://gitlab.com/eggerd_hda/fwe-ha2) (5. Semester, Wahlpflicht)  
  Die Aufgabe bestand in der Full-Stack Entwicklung eines Aktivitätsplaners mit modernem responsive Design unter Verwendung von Angular, Express und MongoDB. Dabei konnte auch auf das Wissen und die Erfahrungen aus *Professionelles Testen* zurückgegriffen werden

- [Professionelles Testen](https://gitlab.com/eggerd_hda/prof_test_server) (5. Semester, Wahlpflicht)  
  Entwicklung eines Webinterfaces zur Visualisierung von Flugpositionsdaten. Ein großer Schwerpunkt lag hierbei natürlich auf der Implementierung möglichst umfangreicher Backend- und Frontend-Tests, um die gelernten Vorgehensweisen und Best Practices zu üben

- [Projekt Systementwicklung](https://gitlab.com/eggerd_hda/pse) (5. Semester)  
  Ein größeres Gruppenprojekt, bei dem eine dezentrale und datenschutzbewusste soziale Plattform entwickelt wurde, bei der die Daten eines Nutzers verschlüsselt in dessen privatem Online-Speicher wie z.B. Nextcloud oder Dropbox gespeichert werden

- [Graphische Datenverarbeitung](https://gitlab.com/eggerd_hda/gdv) (4. Semester)  
  Mithilfe von OpenGL haben wir einen simplistischen Helikopter modelliert und animiert. Dabei wurde das korrekte Zusammenspiel von einzeln animierten Objekten, die gleichzeitig als Einheit einen rechteckigen Kurs abfliegen, geübt

- [Entwicklung nutzerorientierter Anwendungen](https://gitlab.com/eggerd_hda/ena) (3. Semester)  
  Entwicklung einer kleinen Android App zum Bestellen diverser Baustellengeräte. Bei der Gestaltung der App galt es, die gelernten Usability-Kriterien zu beachten, um eine möglichst reibungslose User Experience zu schaffen

- [Mikroprozessorsysteme](https://gitlab.com/eggerd_hda/mps) (2. Semester)  
  In diesem Praktikum haben wir die Steuerung für eine Ausschankstation entwickelt, die aus einer Saitenresonanzwaage und einer Pumpe besteht. Der Schwerpunkt lag dabei auf der Handhabung von Softwareinterrupts und der Integration von Peripheriegeräten
